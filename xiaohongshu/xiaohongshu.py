#! /usr/bin/python3
# -*- coding: utf-8 -*-

# *=*=*=*=*=*=*=*=*=*=*

# author: sugar
# datetime: 2019/3/14 09:21

# *=*=*=*=*=*=*=*=*=*=*
from idataapi_transform import WriterConfig, ProcessFactory


class XiaoHongShu(object):
    def __init__(self):
        writer_config = WriterConfig.WMongoConfig('xiaohongshu', host='10.30.208.25', port=55555,
                                                  database='xiaohongshu',
                                                  username='owner', password='12qwaszx', id_hash_func=self.id_hash_func,
                                                  )
        self.mongo_writer = ProcessFactory.create_writer(writer_config)

    async def save(self, item):
        tmp = []
        for each in item:
            if 'content' in each:
                tmp.append(each)
        if tmp:
            with self.mongo_writer as writer:
                await writer.write(tmp)

    def id_hash_func(self, item):
        if 'id' in item:
            return item['id']


if __name__ == '__main__':
    print('main over')
