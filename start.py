#! /usr/bin/python3
# -*- coding: utf-8 -*-

# *=*=*=*=*=*=*=*=*=*=*

# author: sugar
# datetime: 2019/3/20 10:54

# *=*=*=*=*=*=*=*=*=*=*
import asyncio
from redisScanner import RedisScanner
from Checker import Checker
from CheckerWechat import CheckerWechat


def start():
    scanner = RedisScanner()
    Checker()
    CheckerWechat()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(scanner.scan())


if __name__ == '__main__':
    start()
    print('main over')
