#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 19-1-12 下午5:50
# @Author  : xiaoge
# @File    : SaveWeiXin.py
# @Software: PyCharm


from datetime import datetime
from hashlib import md5

from idataapi_transform import ProcessFactory, WriterConfig
from utils import effective_date
from Checker import Checker


class WX(object):
    def __init__(self):
        self.host = None
        self.port = None
        self.user = None
        self.password = None
        self.database = None
        # self.loger = get_log()
        self.collection = datetime.now().strftime("data%Y%m%d")
        self.checker = Checker()
        mongo_config = WriterConfig.WMongoConfig(self.collection, id_hash_func=self.id_hash_func, filter_=self.filter,
                                                 host='10.30.0.53', port=55555, username='writer', password='writer',
                                                 database='spider')
        self.mongo_writer = ProcessFactory.create_writer(mongo_config)

    async def save(self, data):
        with self.mongo_writer as mongo_writer:
            await mongo_writer.write(data)

    def filter(self, item):
        if 'html' not in item or 'content' not in item:
            return None

        if 'id' in item and item['id']:
            if 'weixinpro' in item['appCode'] and effective_date(item['publishDate'], 3):
                if self.id_hash_func(item) not in self.checker:
                    title = item['title']
                    item['interface'] = 'post/weixinpro'.format(item['appCode'])
                    item['tagId'] = ['22']
                    uid = '%s_%s' % (self.strQ2B(item['title']), item['posterScreenName']) if title else None
                    item['md5_biz'] = self.make_hash(item['url'])
                    item['md5_uid'] = self.make_hash(uid) if uid else None
                    self.checker.add(self.make_hash(item['id']))
                    return item
            elif item['appCode'] == 'weixin' and effective_date(item['publishDate'], 3):
                if self.id_hash_func(item) not in self.checker:
                    title = item['title']
                    item['interface'] = 'post/{}'.format(item['appCode'])
                    item['tagId'] = ['22']
                    uid = '%s_%s' % (self.strQ2B(item['title']), item['posterScreenName']) if title else None
                    item['md5_uid'] = self.make_hash(uid) if uid else None
                    self.checker.add(self.make_hash(item['id']))
                    return item
                else:
                    pass

    def id_hash_func(self, item):
        if 'id' in item:
            value = (item["id"]).encode("utf8")
            return md5(value).hexdigest()

    def make_hash(self, value):
        return md5(value.encode("utf8")).hexdigest()

    def strQ2B(self, ustring):
        """全半角转换"""
        rstring = ""
        for uchar in ustring:
            inside_code = ord(uchar)
            if inside_code == 12288:  # 全角空格直接转换
                inside_code = 32
            elif 65374 >= inside_code >= 65281:  # 全角字符（除空格）根据关系转化
                inside_code -= 65248
            rstring += chr(inside_code)
        return rstring


if __name__ == '__main__':
    print('main over')
