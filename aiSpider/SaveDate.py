#! /usr/bin/python3
# -*- coding: utf-8 -#-
'''
save data to mongoDB
'''
from datetime import datetime
from hashlib import md5

from idataapi_transform import ProcessFactory, WriterConfig
from utils import effective_date
from Checker import Checker


class SaveToDB(object):
    def __init__(self):

        self.collection = datetime.now().strftime("data%Y%m%d")
        self.checker = Checker()
        mongo_config = WriterConfig.WMongoConfig(self.collection, id_hash_func=self.id_hash_func, filter_=self.filter,
                                                 host='10.30.0.53', port=55555, username='writer', password='writer',
                                                 database='spider')
        self.mongo_writer = ProcessFactory.create_writer(mongo_config)

    async def save(self, data):
        with self.mongo_writer as mongo_writer:
            await mongo_writer.write(data)

    def filter(self, item):
        if self.id_hash_func(item) not in self.checker:
            if 'content' in item and item['content']:
                if 'publishDate' in item and effective_date(item['publishDate'], 3):
                    self.checker.add(self.id_hash_func(item))
                    return item
                else:
                    pass
                    # print(item['url'] if 'url' in item else None)
        else:
            pass

    def id_hash_func(self, item):
        if 'url' in item and item['url']:
            value = (item["url"]).encode("utf8")
        else:
            value = str(item).encode("utf8")
        return md5(value).hexdigest()


if __name__ == '__main__':
    print('main over')
