import os
import asyncio
from datetime import datetime, timedelta

from bloom_filter import BloomFilter
from idataapi_transform import ProcessFactory, GetterConfig


class Checker(object):
    def __init__(self, path='/root/nginx/tornado-api/huagui/nginx_tools/nginx_realtimeapi/logs_2/redistomongodb/bloom_checker'):
        """
        "/root/nginx/tornado-api/huagui/nginx_tools/nginx_realtimeapi/logs_2/file_to_mongodb/bloom_checker"
        :param path: file path, save to disk
        """
        self.bloom_filter = None

        now = datetime.now()
        yesterday = now - timedelta(days=1)
        self.yesterday_date_str = yesterday.strftime("data%Y%m%d")
        self.day_before_yesterday_date_str = (yesterday - timedelta(days=1)).strftime("data%Y%m%d")
        self.today_date_str = now.strftime("data%Y%m%d")

        self.path = path + "_" + self.today_date_str
        self.path_yesterday = path + "_" + self.yesterday_date_str
        if not os.path.exists(self.path):
            self.reload()
            if os.path.exists(self.path_yesterday):
                os.unlink(self.path_yesterday)
        else:
            self.load_bloom_filter()

    def load_bloom_filter(self):
        self.bloom_filter = BloomFilter(max_elements=10000000, error_rate=0.01, filename=self.path, start_fresh=False)

    def __contains__(self, item):
        return item in self.bloom_filter

    def add(self, str_object):
        self.bloom_filter.add(str_object)

    def reload(self):
        self.bloom_filter = BloomFilter(max_elements=10000000, error_rate=0.01, filename=self.path, start_fresh=True)
        loop_inner = asyncio.get_event_loop()
        loop_inner.run_until_complete(asyncio.gather(*[self.reload_collection(i) for i in (self.day_before_yesterday_date_str, self.yesterday_date_str, self.today_date_str)]))

    async def reload_collection(self, collection_name):
        getter_config = GetterConfig.RMongoConfig(collection_name)
        getter_config.get_mongo_cli()
        getter_config.cursor = getter_config.client[getter_config.database][getter_config.collection].find({}, {'_id': True})
        getter = ProcessFactory.create_getter(getter_config)
        async for items in getter:
            for item in items:
                self.bloom_filter.add(item["_id"])


if __name__ == "__main__":
    c = Checker()
    r = 'test_199' in c
    print(r)
