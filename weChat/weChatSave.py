#! /usr/bin/python3
# -*- coding: utf-8 -#-
'''
about what
'''
import time
from hashlib import md5
from weChat.collectionConfig import CollectionConfig
from CheckerWechat import CheckerWechat


class SaveWeChat(object):
    def __init__(self):
        self.collections = CollectionConfig()
        checker = CheckerWechat()
        self.wechatpro_checker = checker.wechatpro_checker

    async def save_data(self, data_list, request_time, app_cpde):
        if 'weixinpro' in app_cpde:
            for data in data_list:
                if 'id' in data and self.make_hash(data['id']) not in self.wechatpro_checker:

                    if 'publishDateStr' in data and data['publishDateStr']:
                        title = data['title']
                        uid = '%s_%s' % (self.strQ2B(title), data['posterScreenName']) if title else None
                        date_str = data['publishDateStr'].split('-')[0]
                        data['md5_biz'] = self.make_hash(data['url'])
                        data['md5_uid'] = self.make_hash(uid) if uid else None
                        self.wechatpro_checker.add(self.make_hash(data['id']))
                        print('wechatpro save .......')
                        await self.save(data, date_str)
                else:
                    if 'id' in data and 'updateDate' in data:  # and data['updateDate'] - request_time < 600
                        if 'publishDateStr' in data and data['publishDateStr']:
                            title = data['title']
                            uid = '%s_%s' % (self.strQ2B(title), data['posterScreenName']) if title else None
                            date_str = data['publishDateStr'].split('-')[0]
                            data['md5_biz'] = self.make_hash(data['url'])
                            data['md5_uid'] = self.make_hash(uid) if uid else None
                            print('wechatpro update .....')
                            await self.save(data, date_str)
                    else:
                        print(data['updateDate'], self.format_time(request_time))

        elif app_cpde == 'weixin':
            for data in data_list:
                if 'id' in data and self.make_hash(data['id']) not in self.wechatpro_checker:
                    if 'publishDateStr' in data and data['publishDateStr']:
                        title = data['title']
                        uid = '%s_%s' % (self.strQ2B(data['title']), data['posterScreenName']) if title else None
                        date_str = data['publishDateStr'].split('-')[0]
                        data['md5_uid'] = self.make_hash(uid) if uid else None
                        print('wechat save .......')
                        await self.save(data, date_str)

    async def save(self, data, date_str):
        if date_str in self.collections.config_map:
            # data.pop('interface', None)
            # data.pop('tagId', None)
            with self.collections.config_map[date_str] as w:
                await w.write([data])

    def make_hash(self, value):
        return md5(value.encode("utf8")).hexdigest()

    def format_time(self, date_str):
        return int(time.mktime(time.strptime(date_str, '%Y/%m/%d %H:%M:%S')))

    def strQ2B(self, ustring):
        """全半角转换"""
        rstring = ""
        for uchar in ustring:
            inside_code = ord(uchar)
            if inside_code == 12288:  # 全角空格直接转换
                inside_code = 32
            elif 65374 >= inside_code >= 65281:  # 全角字符（除空格）根据关系转化
                inside_code -= 65248
            rstring += chr(inside_code)
        return rstring


if __name__ == '__main__':

    print('main over')
