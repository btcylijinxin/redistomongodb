#! /usr/bin/python3
# -*- coding: utf-8 -*-

# *=*=*=*=*=*=*=*=*=*=*

# author: sugar
# datetime: 2019/4/1 18:05

# *=*=*=*=*=*=*=*=*=*=*
from idataapi_transform import WriterConfig, ProcessFactory


class Profile(object):
    def __init__(self):
        writer_config = WriterConfig.WMongoConfig('wechatProfile', host='10.30.208.25', port=55555,
                                                  database='wechat',
                                                  username='owner', password='12qwaszx', id_hash_func=self.id_hash_func,
                                                  )
        self.mongo_writer = ProcessFactory.create_writer(writer_config)

    async def save(self, item):
        with self.mongo_writer as writer:
            await writer.write(item)

    def id_hash_func(self, item):
        return item['id']
