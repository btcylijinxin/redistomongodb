#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 19-1-22 上午11:24
# @Author  : xiaoge
# @File    : collectionConfig.py
# @Software: PyCharm

from hashlib import md5
from idataapi_transform import WriterConfig, ProcessFactory

from utils import get_config


wechat_config = get_config(section='wechat')
HOST = wechat_config['host']
PORT = wechat_config['port']
USER = wechat_config['user']
PWD = wechat_config['password']
DB = wechat_config['db']


class CollectionConfig(object):
    def __init__(self):
        mongo_config1 = WriterConfig.WMongoConfig('wechat2014', host=HOST, port=int(PORT), database=DB, username=USER,
                                                  password=PWD, id_hash_func=self.id_hash_func)
        self.mongo_writer1 = ProcessFactory.create_writer(mongo_config1)

        mongo_config2 = WriterConfig.WMongoConfig('wechat2015', host=HOST, port=int(PORT), database=DB, username=USER,
                                                  password=PWD, id_hash_func=self.id_hash_func)
        self.mongo_writer2 = ProcessFactory.create_writer(mongo_config2)

        mongo_config3 = WriterConfig.WMongoConfig('wechat2016', host=HOST, port=int(PORT), database=DB, username=USER,
                                                  password=PWD, id_hash_func=self.id_hash_func)
        self.mongo_writer3 = ProcessFactory.create_writer(mongo_config3)

        mongo_config4 = WriterConfig.WMongoConfig('wechat2017', host=HOST, port=int(PORT), database=DB, username=USER,
                                                  password=PWD, id_hash_func=self.id_hash_func)
        self.mongo_writer4 = ProcessFactory.create_writer(mongo_config4)

        mongo_config5 = WriterConfig.WMongoConfig('wechat2018', host=HOST, port=int(PORT), database=DB, username=USER,
                                                  password=PWD, id_hash_func=self.id_hash_func)
        self.mongo_writer5 = ProcessFactory.create_writer(mongo_config5)

        mongo_config6 = WriterConfig.WMongoConfig('wechat2019', host=HOST, port=int(PORT), database=DB, username=USER,
                                                  password=PWD, id_hash_func=self.id_hash_func)
        self.mongo_writer6 = ProcessFactory.create_writer(mongo_config6)

        self.config_map = {
            '2014': self.mongo_writer1,
            '2015': self.mongo_writer2,
            '2016': self.mongo_writer3,
            '2017': self.mongo_writer4,
            '2018': self.mongo_writer5,
            '2019': self.mongo_writer6,
        }

    def id_hash_func(self, data):
        value = (data["id"]).encode("utf8")
        return md5(value).hexdigest()
