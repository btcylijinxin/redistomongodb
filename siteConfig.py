#! /usr/bin/python3
# -*- coding: utf-8 -#-
'''
about what
'''


blacklist = [
    'tonghuashun', 'weibo', 'tvbc', 'sogou', 'jike', 'baidu',
    'weiboweb', 'qihoo', 'xiaohongshu_ids'
]

SiteConfig = {

    # 重点媒体
    'renminribao': {'domain': 'app.peopleapp.com', 'top_domain': 'peopleapp.com', 'interface': 'news/renminribao', 'isOverseas': 0, 'name': '人民日报', 'siteId': 17627, 'siteOriginId': 5746, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'eastday': {'domain': 'mini.eastday.com', 'top_domain': 'eastday.com', 'interface': 'news/eastday', 'isOverseas': 0, 'name': '东方头条', 'siteId': 15485, 'siteOriginId': 5747, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'cntv': {'domain': 'news.cctv.com', 'top_domain': 'cctv.com', 'interface': 'news/cntv', 'isOverseas': 0, 'name': '央视新闻', 'siteId': 57, 'siteOriginId': 5748, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'cctvplus': {'domain': 'www.newscctv.net', 'top_domain': 'newscctv.net', 'interface': 'news/cctvplus', 'isOverseas': 0, 'name': '央视新闻plus', 'siteId': 17626, 'siteOriginId': 5749, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'yidianzixun': {'domain': 'www.yidianzixun.com', 'top_domain': 'yidianzixun.com', 'interface': 'news/yidianzixun', 'isOverseas': 0, 'name': '一点资讯', 'siteId': 17102, 'siteOriginId': 5750, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['12', '23'], 'timezone': 8},
    'thepaper': {'domain': 'www.thepaper.cn', 'top_domain': 'thepaper.cn', 'interface': 'news/thepaper', 'isOverseas': 0, 'name': '澎湃新闻', 'siteId': 17103, 'siteOriginId': 5751, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'wangyi': {'domain': 'news.163.com', 'top_domain': '163.com', 'interface': 'news/wangyi', 'isOverseas': 0, 'name': '网易新闻', 'siteId': 17104, 'siteOriginId': 5752, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'sohu': {'domain': 'news.sohu.com', 'top_domain': 'sohu.com', 'interface': 'news/sohu', 'isOverseas': 0, 'name': '搜狐新闻', 'siteId': 15854, 'siteOriginId': 5753, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'toutiao': {'domain': 'www.toutiao.com', 'top_domain': 'toutiao.com', 'interface': 'news/toutiao', 'isOverseas': 0, 'name': '今日头条', 'siteId': 17108, 'siteOriginId': 5754, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['22', '23'], 'timezone': 8},
    'tencent': {'domain': 'news.qq.com', 'top_domain': 'qq.com', 'interface': 'news/tencent', 'isOverseas': 0, 'name': '腾讯新闻', 'siteId': 15770, 'siteOriginId': 5755, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'ifeng': {'domain': 'news.ifeng.com', 'top_domain': 'ifeng.com', 'interface': 'news/ifeng', 'isOverseas': 0, 'name': '凤凰网', 'siteId': 15654, 'siteOriginId': 5757, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['12', '23'], 'timezone': 8},
    'ttkb': {'domain': 'kuaibao.qq.com', 'top_domain': 'qq.com', 'interface': 'news/ttkb', 'isOverseas': 0, 'name': '天天快报', 'siteId': 15770, 'siteOriginId': 6260, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['23'], 'timezone': 8},
    'sina': {'domain': 'news.sina.cn', 'top_domain': 'sina.cn', 'interface': 'news/sina', 'isOverseas': 0, 'name': '新浪新闻', 'siteId': 16033, 'siteOriginId': 6269, 'siteType': 6, 'sourceRegion': '中国', 'sourceType': 'APP', 'tagId': ['12', '23'], 'timezone': 8},

    # 百度贴吧
    'baidutieba': {'domain': 'tieba.baidu.com', 'top_domain': 'tieba.baidu.com', 'interface': 'post/baidutieba', 'isOverseas': 0, 'name': '百度贴吧文章', 'siteId': 19492, 'siteOriginId': 7795, 'siteType': 1, 'sourceRegion': '中国', 'sourceType': '贴吧', 'tagId': ['12'], 'timezone': 8},


    # 'eastday': {'sourceType': 'APP', 'sourceRegion': '中国', 'top_domain': 'eastday.com', 'siteOriginId': 4337},
    'weixinpro': {'appCode': 'weixinpro' ,'sourceType': 'APP', 'sourceRegion': '中国', 'top_domain': 'weixinpro', 'siteOriginId': 6352, 'name': '微信公众号专业版'},
    'weixinpro2': {'appCode': 'weixinpro' ,'sourceType': 'APP', 'sourceRegion': '中国', 'top_domain': 'weixinpro', 'siteOriginId': 6352, 'name': '微信公众号专业版'},
    'weixinpro3': {'appCode': 'weixinpro' ,'sourceType': 'APP', 'sourceRegion': '中国', 'top_domain': 'weixinpro', 'siteOriginId': 6352, 'name': '微信公众号专业版'},
    'weixinpro4': {'appCode': 'weixinpro' ,'sourceType': 'APP', 'sourceRegion': '中国', 'top_domain': 'weixinpro', 'siteOriginId': 6352, 'name': '微信公众号专业版'},

    'weixin': {'appCode': 'weixin' ,'sourceType': 'APP', 'sourceRegion': '中国', 'top_domain': 'weixin', 'siteOriginId': 6342, 'name': '微信公众号'},

    'xiaohongshu': {'appCode': 'xiaohongshu', 'sourceType': 'APP', 'sourceRegion': '中国', 'top_domain': 'xiaohongshu.com', 'siteOriginId': 6350, 'name': '小红书'},

    }

if __name__ == '__main__':
    print('main over')
