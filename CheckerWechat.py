import os
import logging
import asyncio
from datetime import datetime, timedelta

from bloom_filter import BloomFilter
from idataapi_transform import ProcessFactory, GetterConfig


class Checker(object):
    def __init__(self, path="/root/nginx/tornado-api/huagui/nginx_tools/nginx_realtimeapi/logs_2/redistomongodb/bloom_checker", pro=False):
        """
        :param path: file path, save to disk
        """
        self.bloom_filter = None

        app_code = "weixinpro" if pro else "weixin"
        self.path = path + "_" + app_code
        self.load_bloom_filter()

    def load_bloom_filter(self):
        self.bloom_filter = BloomFilter(max_elements=100000000, error_rate=0.001, filename=self.path, start_fresh=False)

    def __contains__(self, item):
        return item in self.bloom_filter

    def add(self, str_object):
        self.bloom_filter.add(str_object)

    def reload(self):
        self.bloom_filter = BloomFilter(max_elements=100000000, error_rate=0.001, filename=self.path, start_fresh=True)


class CheckerWechat(object):
    def __init__(self):
        now = datetime.now()
        three_years_ago = now - timedelta(days=365 * 3, seconds=10)
        self.year_str_lst = list()

        while three_years_ago < now:
            self.year_str_lst.append(three_years_ago.strftime("wechat%Y"))
            three_years_ago += timedelta(days=365)

        self.wechat_checker = Checker(pro=False)
        self.wechatpro_checker = Checker(pro=True)

    def reload(self):
        self.wechat_checker.reload()
        self.wechatpro_checker.reload()
        loop_inner = asyncio.get_event_loop()
        loop_inner.run_until_complete(
            asyncio.gather(*[self.reload_collection(coll_name) for coll_name in self.year_str_lst]))

    async def reload_collection(self, collection_name):
        getter_config = GetterConfig.RMongoConfig(collection_name, host='10.30.208.25', port=55555, database='wechat',
                                                  username='owner', password='12qwaszx')
        getter_config.get_mongo_cli()
        getter_config.cursor = getter_config.client[getter_config.database][getter_config.collection].find({},
                                                                                                           {'_id': True,
                                                                                                            "appCode": True})
        getter = ProcessFactory.create_getter(getter_config)
        async for items in getter:
            for item in items:
                if item["appCode"] == "weixin":
                    self.wechat_checker.bloom_filter.add(item["_id"])
                elif item["appCode"] == "weixinpro":
                    self.wechatpro_checker.bloom_filter.add(item["_id"])
                else:
                    logging.error("unknown appCode: %s" % (item["appCode"],))


if __name__ == "__main__":
    c = CheckerWechat()
    c.reload()
    print("7ee7c24999ba9265b245241f1fd3fa3e" in c.wechatpro_checker)
    print("7ee7c24999ba9265b245241f1fd3fa3e" in c.wechat_checker)
