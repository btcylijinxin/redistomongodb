#! /usr/bin/python3
# -*- coding: utf-8 -*-

# *=*=*=*=*=*=*=*=*=*=*

# author: sugar
# datetime: 2019/3/20 08:59

# *=*=*=*=*=*=*=*=*=*=*
import sys
import asyncio
import json
import time
from datetime import datetime

from utils import get_redis
from siteConfig import blacklist, SiteConfig
from weChat.weChatSave import SaveWeChat
from weChat.saveProfile import Profile
from xiaohongshu.xiaohongshu import XiaoHongShu
from aiSpider.SaveDate import SaveToDB
from aiSpider.SaveWeiXin import WX


class RedisScanner(object):
    def __init__(self):

        self.type = ['post',
                     'news',
                     'profile',
                     ]

        self.redis = get_redis()

        # 缓存
        self.wechat = SaveWeChat()
        self.xiaohongshu = XiaoHongShu()

        # 通用
        self.weixin = WX()
        self.save = SaveToDB()

        # profile
        self.profile = Profile()

    async def scan(self):
        flag = datetime.today().strftime('%Y-%m-%d')
        while True:
            today = datetime.today().strftime('%Y-%m-%d')
            if today != flag:
                sys.exit()
            obj = self.redis.lpop('ngx_data')
            if obj:
                item = json.loads(obj.decode())
                if item['dataType'] in self.type:
                    await self.data_filter(item)
            else:
                print('暂无数据，请等待五秒钟')
                time.sleep(5)

    async def data_filter(self, item):
        app_code = item['appCode']
        data_type = item['dataType']
        if data_type == 'profile':
            if 'weixin' in app_code:
                if 'data' in item and item['data']:
                    data_list = item['data']
                    await self.profile.save(data_list)
        else:
            request_time = item['logDate']
            if 'data' in item and item['data'] and app_code not in blacklist:
                data_list = item['data']
                if app_code in SiteConfig:
                    # if app_code in ['renminribao', 'eastday', 'cntv', 'cctvplus', 'yidianzixun', 'thepaper', 'wangyi', 'sohu', 'toutiao', 'tencent', 'ifeng', 'ttkb', 'sina']:
                    #     if data_type != 'comment':
                    #         tmp = []
                    #         for data in data_list:
                    #             data.update(SiteConfig[app_code])
                    #             tmp.append(data)
                    #         await self.save.save(tmp)
                    # else:
                    tmp = []
                    for data in data_list:
                        if 'interface' in SiteConfig[app_code]:
                            data['interface'] = SiteConfig[app_code]['interface']
                        if 'appCode' in SiteConfig[app_code]:
                            data['appCode'] = SiteConfig[app_code]['appCode']
                        data['top_domain'] = SiteConfig[app_code]['top_domain']
                        data['sourceType'] = SiteConfig[app_code]['sourceType']
                        data['sourceRegion'] = SiteConfig[app_code]['sourceRegion']
                        data['siteOriginId'] = int(SiteConfig[app_code]['siteOriginId'])
                        data['name'] = SiteConfig[app_code]['name']
                        data['createDate'] = int(time.time() * 1000)
                        tmp.append(data)
                    if 'weixin' in app_code:
                        # # 微信mongodb存储
                        # await self.wechat.save_data(tmp, request_time, app_code)
                        # # 通用爬虫存储微信数据
                        # await self.weixin.save(tmp)

                        tasks = [
                            asyncio.ensure_future(self.wechat.save_data(tmp, request_time, app_code)),
                            asyncio.ensure_future(self.weixin.save(tmp))
                        ]
                        await asyncio.gather(*tasks)

                    elif 'xiaohongshu' in app_code:
                        # await self.xiaohongshu.save(tmp)
                        # await self.save.save(tmp)

                        tasks = [
                            asyncio.ensure_future(self.xiaohongshu.save(tmp)),
                            asyncio.ensure_future(self.save.save(tmp))
                        ]
                        await asyncio.gather(*tasks)
                    else:
                        # 存储其他数据
                        await self.save.save(tmp)
            else:
                return None


if __name__ == '__main__':
    scanner = RedisScanner()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(scanner.scan())
    print('main over')
