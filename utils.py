#! /usr/bin/python3
# -*- coding: utf-8 -*-

# *=*=*=*=*=*=*=*=*=*=*

# author: sugar
# datetime: 2019/3/19 17:41

# *=*=*=*=*=*=*=*=*=*=*

import time
from datetime import datetime, timedelta
from redis import Redis
from pymongo import MongoClient
from configparser import ConfigParser


def get_config(file_path='./baseConfig.ini', section='redis'):
    config = ConfigParser()
    config.read(file_path)
    host = config.get(section, 'host')
    port = config.get(section, 'port')
    user = config.get(section, 'user')
    password = config.get(section, 'password')
    if config.has_option(section, 'db'):
        db = config.get(section, 'db')
    else:
        db = ''
    return {
        'host': host,
        'port': port,
        'user': user,
        'password': password,
        'db': db,
    }


def get_redis():
    redis_config = get_config()
    return Redis(host=redis_config['host'], port=int(redis_config['port']),
                 password=redis_config['password'], db=int(redis_config['db']))


# filter data which published time more then three days
def effective_date(timestamp, day):
    if timestamp:
        now = datetime.now()
        before = (now - timedelta(day)).strftime("%Y-%m-%dT%H:%M:%S")
        before_time = int(time.mktime(time.strptime(before, '%Y-%m-%dT%H:%M:%S')))
        if timestamp >= before_time:
            return True
        else:
            return False
    else:
        return False


def today_published(timestamp):
    if timestamp:
        now = datetime.now()
        before = (now - timedelta(1)).strftime("%Y-%m-%dT00:00:00")
        before_time = int(time.mktime(time.strptime(before, '%Y-%m-%dT00:00:00')))
        if timestamp >= before_time:
            return True
        else:
            return False
    else:
        return False


def get_site_config():
    mongo = MongoClient(host='120.79.97.220', port=55555)
    db = mongo['spider']
    db.authenticate(name='reader', password='reader')
    col = db['sites']
    all_site = col.find(
        {"_id": {"$in": [6269, 6260, 5757, 5755, 5754, 5753, 5752, 5751, 5750, 5749, 5748, 5747, 5746]}})
    names = []
    for site in all_site:
        res = {}
        tmp = {}
        # print(site)
        name = site['interface'].split('/')[1]
        tmp['domain'] = site['domain']
        tmp['top_domain'] = site['top_domain']
        tmp['interface'] = site['interface']
        tmp['isOverseas'] = site['isOverseas']
        tmp['name'] = site['name']
        tmp['siteId'] = site['siteId']
        tmp['siteOriginId'] = site['_id']
        tmp['siteType'] = site['siteType']
        tmp['sourceRegion'] = site['sourceRegion'] if 'sourceRegion' in site else None
        tmp['sourceType'] = site['sourceType']
        tmp['tagId'] = site['tagId']
        tmp['timezone'] = site['timezone']
        res[name] = tmp
        names.append(name)
    print(names)


if __name__ == '__main__':
    # redis = get_redis()
    # print(redis.keys())
    get_site_config()
    print('main over')
